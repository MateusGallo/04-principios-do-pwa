# 04 - Principios do PWA

[Progressive Web Apps](https://developers.google.com/web/progressive-web-apps/) são, sucintamente falando, aplicativos criados em navegadores. À medida que o usuário constrói progressivamente um relacionamento com o aplicativo ao longo do tempo, ele se torna mais e mais poderoso. Ele é carregado rapidamente, mesmo em redes escamosas, envia notificações push relevantes, tem um ícone na tela inicial e é carregado como uma experiência de tela inteira de nível superior.

Então, um PWA é:

- **Progressivo** - Funciona para todos os usuários, independentemente da escolha do navegador, porque é construído com aprimoramento progressivo como um princípio básico.
- **Responsivo** - Compatível com qualquer fator de forma: desktop, celular, tablet ou o que for o próximo.
- **Independente de conectividade** - Aprimorado com `service workers` para trabalhar offline ou em redes de baixa qualidade.
- **App-like** - Parece um aplicativo, porque o modelo de shell do aplicativo separa a funcionalidade do conteúdo.
- **Fresh** - Sempre atualizado graças ao processo de atualização do `service worker`.
- **Seguro** - veiculado via HTTPS para evitar espionagem e garantir que o conteúdo não tenha sido adulterado.
- **Detectável** - É identificável como um "aplicativo" graças ao escopo do registro do W3C `manifest` e `service worker`, permitindo que os mecanismos de pesquisa o encontrem.
- **Reengajável** - Facilita o reengajamento por meio de recursos como `notificações push`.
- **Instalável** - permite que os usuários adicionem aplicativos mais úteis a sua tela inicial sem o incômodo de uma loja de aplicativos.
- **Linkable** - Compartilhe facilmente o aplicativo via URL, não requer instalação complexa.

Para que um PWA funcione sem mais problemas, devemos utilizar dois arquivos importantes:

1. `serviceWorker.js`
2. `manifest.json`

### Docs

- https://developers.google.com/web/progressive-web-apps/
- https://codelabs.developers.google.com/codelabs/your-first-pwapp/#2


## Service Workers

Um service worker é um script que seu navegador executa em segundo plano, separado da página da Web. Isso possibilita recursos que não precisam de uma página da Web ou de interação do usuário. Atualmente, eles já incluem recursos como `notificações push` e `sincronização em segundo plano`. No futuro, os service workers permitirão outras ações como sincronização periódica ou fronteira geográfica virtual. O principal recurso discutido neste tutorial é a capacidade de interceptar e gerenciar solicitações de rede, incluindo o gerenciamento programático de um cache de respostas.

Características importantes de um service worker:

- Ele é um JavaScript Worker, portanto, não consegue acessar o DOM diretamente. Em vez disso, um service worker pode se comunicar com as páginas que controla respondendo a mensagens enviadas pela interface `postMessage`. Essas páginas podem manipular o DOM, se necessário.
- O service worker é um proxy de rede programável, o que permite controlar como as solicitações de rede da página são gerenciadas.
- Ele é encerrado quando ocioso e reiniciado quando necessário novamente. Isso significa que não se pode confiar no estado global dentro dos gerenciadores `onfetch` e `onmessage` de um service worker. Para informações que devem ser mantidas e reutilizadas entre reinícios, os service workers podem acessar a `IndexedDB API`.
- Os service workers usam muito `Promises`. Se você não estiver familiarizado com isso, interrompa esta leitura e confira [Promessas, uma introdução](https://developers.google.com/web/fundamentals/primers/promises?hl=pt-br).

### Ciclo de vida

O ciclo de vida do service worker é a parte mais complicada. Se você não souber o que ele está tentando fazer nem os benefícios que ele traz, pode parecer que ele só atrapalha. Mas ao entender como ele funciona, é possível oferecer atualizações fáceis e discretas aos usuários, combinando o melhor da Web com o melhor dos padrões nativos.

[Fluxo do service worker:](https://developer.mozilla.org/pt-BR/docs/Web/API/Service_Worker_API#Download_instala%C3%A7%C3%A3o_e_ativa%C3%A7%C3%A3o)

- O evento install é o primeiro evento que um service worker recebe e só acontece uma vez.
- Uma promessa passada a installEvent.waitUntil() sinaliza a duração e o êxito ou uma falha na instalação.
- Um service worker não receberá eventos como fetch e push até finalizar com sucesso a instalação e ficar "ativo".
- Por padrão, as buscas de uma página não passam por um service worker a menos que a solicitação da página tenha passado por um. Por isso, você precisaria atualizar a página para ver os efeitos do service worker.
- clients.claim() pode modificar esse padrão e assumir o controle de páginas não controladas.

### Docs

- https://developers.google.com/web/fundamentals/primers/service-workers/lifecycle?hl=pt-br

## Web App Manifest

O manifesto dos PWAs é um arquivo JSON que permite controlar como o aplicativo ou site é exibido para o usuário em áreas que normalmente se espera ver em aplicativos nativos (por exemplo, a tela inicial de um dispositivo), como definir o que o usuário pode inicializar e o visual durante a inicialização.

Quando criar o manifesto e ele estiver no seu site, adicione uma tag link a todas as páginas que envolve o seu aplicativo web da seguinte forma:

``` html
<link rel="manifest" href="/manifest.json">
```

### Doc

- https://developers.google.com/web/fundamentals/web-app-manifest/?hl=pt-br
- https://developer.mozilla.org/pt-BR/docs/Web/Manifest

# Desafio

Diferente de outros desafios, esse exigirá de vocês mais pesquisa que a mão na massa. A pasta do desafio já esta pronta com o `serviceWorker.js` na pasta `src` e a pasta `public` já contem um `manifest.json` prontos, porem cabe a vocês a fazer as seguintes alterações:

- Mude as cores principais do Aplicativo;
- Altere a "splash page" do App conforme seu gosto;
- Adicione um meio de quando a página for acessada aparecer uma opção para adicionar o App na tela inicial;
- Faça com que a página consiga ser acessada sem acesso à internet (destaque-se exibindo uma mensagem de que não há conexão);
- Crie um método de notificação push ao estar na visualização do App.

Após esses pontos serem completos. Reuna tudo em seu próprio App! Avaliaremos todo o conteúdo aprendido até aqui, Componentes, props, states, elevação do state, formulários e keys. Quanto mais você utilizar da maneira correta, melhor será sua avaliação.

Essa será a ultima entrega da primeira parte do curso. Faça o seu melhor e destaque-se dentre os outros, utilize o Slack tanto para tirar dúvidas quanto para ajudar seus colegas.

Lembre-se: Organização de código é uma **obrigação**, então indente bem o código, tente utilizar extensões do VS Code que possam te ajudar a ter um código bem indentado.

Inicialização:
- De um [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) no repositório: https://gitlab.com/acct.fateclab/turma-1-sem-2019/04-principios-do-pwa;
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com **HTTPS**. 
- Agora localmente abra uma pasta e use o botão direito do Mouse para abrir o "Git Bash", com esse atalho você chegará na pasta que quer mais rapidamente pelo terminal.
- Use o comando `git clone url-copiada-do-gitlab` para que a estrutura de pastas do repositório seja clonada na sua pasta

Entrega:
- Assim que terminar dê `git push origin seu-nome/basico-dos-componentes`
- Acesse o menu "Merge Requests", configure o "Target Branch" para o [repositório original](https://gitlab.com/acct.fateclab/turma-1-sem-2019/04-principios-do-pwa) para que seu App seja avaliado e revisado e para que possamos te dar um feedback.
- O nome do Merge Request deve ser o seu nome completo.
- Crie o Merge Request

Dica: Após usar o comando `npm start` você conseguirá acessar o App pelo seu celular na URL após "On Your Network".

![Capturar](https://gitlab.com/acct.fateclab/turma-1-sem-2019/02-basico-dos-componentes/uploads/d17c330b64110108843055c440911d56/Capturar.PNG)

![WhatsApp_Image_2019-04-08_at_22.44.29](https://gitlab.com/acct.fateclab/turma-1-sem-2019/02-basico-dos-componentes/uploads/7d1f8b7e27f3e77232ca3661072baa8c/WhatsApp_Image_2019-04-08_at_22.44.29.jpeg)